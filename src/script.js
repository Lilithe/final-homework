function callback1(data) {
    let count = 0;
    for(i = 0; i < data.length; i++) {
        count += data[i];
    }
    return count
}
function callback2(data) {
    let count = 1;
    for(i = 0; i < data.length; i++) {
        count *= data[i];
    }
    return count
}
function w(s, fn) {
    let str = s.split(/(\s+)/);
    let obj = str.filter(str => str.trim().length > 0);
    let arr = Object.values(obj);
    let finalArr = [];
    for(i = 0; i < arr.length; i++) {
        finalArr.push(arr[i].length)
    }
    return fn(finalArr);
}
console.log(w('a bb ccc dddd', callback1));
console.log(w('a bb ccc dddd', callback2))

Array.prototype.shuffle = function() {
    let input = this;
    for (let i = input.length - 1; i >= 0; i--) {
        let randomIndex = Math.floor(Math.random() * (i + 1));
        let itemAtIndex = input[randomIndex];
        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
}
console.log([1,2,3,4].shuffle())

function numberOfDuplicates(arr) {
    const obj = {};
    let value = 0;
    let newArr = [];
    for (i = 0; i < arr.length; i++) {
      value = arr[i];
      if (obj[value]){
         obj[value] = obj[value] + 1;
      }
      else{
         obj[value] = 1;
      }
      newArr.push(obj[value]);
    }
    return newArr
  }
  console.log(numberOfDuplicates([1, 2, 1, 1, 3]))

class Logger {
    loggerArr = new Array();
    log(data) {
        this.loggerArr.push(data);
        return console.log(data);
    }
    getLog() {
        return console.log(this.loggerArr);
    }
    clearLog() {
        this.loggerArr = [];
    }
}  
const logger = new Logger();
logger.log('Event 1');
logger.log('Event 2');
logger.getLog(); // ['Event 1', 'Event 2']
logger.clearLog();
logger.getLog(); // []